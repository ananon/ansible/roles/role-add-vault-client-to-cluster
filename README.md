## add_client_to_cluster.yaml
* This repository contains a role related to HashiCorp Vault.
* Prerequisites
	* `OC` installed on the server.
	*  `Ansible` installed on the server.
	* `ansible-modules-hashivault` Python module installed on the server.

 * The playbook is used when we would like to allow using external-secrets in a project.
 * The playbook creates: 
   * 2 annotations in the namespace
   * K8S Vault role for the client

| Variable | Description | Type | Default | Required |
| ------------- |:-----------------:| -----:|:-------------:| -----:|
| vault_url    | URL of the Vault | String | "" | True
| vault_username     | Username to the Vault      |   String | "" | True
| vault_password | Password to the Vault      |    String | "" | True
| cluster_name | Name of the cluster      |    String | "" | True
| client_name| Name of the cluster      |    String | "" | True
| operator_service_account | ServiceAccount of the operator      |    String | "" | True
| operator_namespace | Namespace of the operator      |    String | "" | True
